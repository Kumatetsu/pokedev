import React, {useState} from 'react'
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Button from '@material-ui/core/Button';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import { IdentifiedType } from '../models/utils';
import { Element } from '../models/elements';
import { Skill } from '../models/skills';
import { configInit } from '../utils/httpUtils';


interface props {
    elements: IdentifiedType<Element>[]
    skills: IdentifiedType<Skill>[]
}

interface State {
    firstname: string;
    lastname: string;
    elements: string[]
    skills: string[]
}

const Component = (props:props) => {
    const state:State =  {
        firstname: '',
        lastname:  '',
        elements:  [],
        skills:    []
    }
    const [formDev, setFormDev] = useState(state);
    const [hasError, setErrors] = useState(false);

    const handleChange = (name: keyof State) => (event: React.ChangeEvent<HTMLInputElement>) => {
        setFormDev({ ...formDev, [name]: event.target.value });
    };

    const handleSelectChange = (name: keyof State) => (event: React.ChangeEvent<{
        name?: string | undefined;
        value: unknown;
    }>) => {
        setFormDev({ ...formDev, [name]: event.target.value });
    };

    async function saveDave(event:any) {
        event.preventDefault()
        let elements = formDev.elements.map(function(element){
            return props['elements'].find(function(objectElement) {
                return objectElement.name === element
            })
        })
        let skills = formDev.skills.map(function(element){
            return props['skills'].find(function(objectElement) {
                return objectElement.name === element
            })
        })

        let boxToApi= Object.assign({}, formDev, {'skills': skills, 'elements':elements });

        fetch("http://localhost:8001/devs",
        configInit(boxToApi)
        ).then(result => result.json())
        .then()
        .catch(err => setErrors(err));
    }

    return (
        <form noValidate autoComplete="off" className="formDev" onSubmit={saveDave}>
            <FormControl>
                <InputLabel htmlFor="lastname">Nom</InputLabel>
                <Input id="lastname" value={formDev.lastname}
                    onChange={handleChange('lastname')}/>
            </FormControl>
            <FormControl>
                <InputLabel htmlFor="firstname">Prénom</InputLabel>
                <Input id="firstname" value={formDev.firstname} onChange={handleChange('firstname')}/>
            </FormControl>
            <FormControl>
                <InputLabel htmlFor="elements">Elements</InputLabel>
                <Select
                    multiple
                    value={formDev.elements}
                    onChange={handleSelectChange('elements')}
                >
                {props.elements.map( (element:IdentifiedType<Element>) => (
                    <MenuItem key={element.id} value={element.name}>
                      {element.name}
                    </MenuItem>
                ))}
                </Select>
            </FormControl>
            <FormControl>
                <InputLabel htmlFor="skills">Skills</InputLabel>
                <Select
                    multiple
                    value={formDev.skills}
                    onChange={handleSelectChange('skills')}
                >
                {props.skills.map( (skill:IdentifiedType<Skill>) => (
                    <MenuItem key={skill.id} value={skill.name}>
                      {skill.name}
                    </MenuItem>
                ))}
                </Select>
            </FormControl>
            <Button type="submit">Enregister</Button>
        </form>
    )
}

export default Component
