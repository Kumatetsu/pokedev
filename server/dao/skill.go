package dao

import (
	"errors"
	"pokedev/server/models"

	"github.com/jinzhu/gorm"
)

//SkillDao gives dao methods for Element
type SkillDao struct {
}

//GetOneByID Get one skill By ID
func (*SkillDao) GetOneByID(db *gorm.DB, id int) (models.Skill, error) {
	var skill models.Skill
	err := db.First(&skill, id).Error
	return skill, err
}

//Update update skill and return it
func (*SkillDao) Update(db *gorm.DB, skill models.Skill) (models.Skill, error) {
	err := db.Save(&skill).Error
	return skill, err
}

//Delete will delete skill from db
func (*SkillDao) Delete(db *gorm.DB, id int) error {
	var skill models.Skill

	err := db.First(&skill, id).Error
	if nil != err {
		return err
	}

	err = db.Delete(&skill).Error
	return err
}

//Create will create a new skill in db
func (*SkillDao) Create(db *gorm.DB, skill models.Skill) (models.Skill, error) {
	if true != db.NewRecord(skill) {
		return skill, errors.New("skill already exists")
	}

	err := db.Create(&skill).Error
	return skill, err
}

//All will return all skills in db
func (*SkillDao) All(db *gorm.DB) ([]models.Skill, error) {
	skills := []models.Skill{}
	err := db.Find(&skills).Error
	return skills, err
}
