package unit

import (
	"encoding/json"
	"pokedev/server/dao"
	"pokedev/server/models"
	"pokedev/server/tests/datas"
	"pokedev/server/tests/utils"
	"testing"
)

var elementDao = &dao.ElementDao{}

func TestDAOGetAllElements(t *testing.T) {
	result, error := elementDao.All(a.Db)
	if error != nil {
		t.Errorf("get all elements failed: %s", error.Error())
	}

	jsonResult, err := json.Marshal(result)
	if err != nil {
		t.Errorf("Result to json failed: %s", err.Error())
	}

	expected, err := json.Marshal(datas.AllElements)
	if err != nil {
		t.Errorf("Element to json failed: %s", err.Error())
	}

	utils.CompareStrings(t, string(jsonResult), string(expected))
}

func TestDaoGetElementByID(t *testing.T) {
	result, error := elementDao.GetOneByID(a.Db, 1)
	if error != nil {
		t.Errorf("get one by ID Element failed: %s", error.Error())
	}

	jsonResult, err := json.Marshal(result)
	if err != nil {
		t.Errorf("Result to json failed: %s", err.Error())
	}

	expected, err := json.Marshal(datas.DevElem)
	if err != nil {
		t.Errorf("Element to json failed: %s", err.Error())
	}

	utils.CompareStrings(t, string(jsonResult), string(expected))
}

func TestDaoCreateElement(t *testing.T) {
	var realNewElement models.Element
	err := json.Unmarshal(datas.RequestNewElement, &realNewElement)
	if err != nil {
		t.Errorf("Unmarshal failed: %s", err.Error())
	}

	result, error := elementDao.Create(a.Db, realNewElement)
	if error != nil {
		t.Errorf("create element failed: %s", error.Error())
	}

	jsonResult, err := json.Marshal(result)
	if err != nil {
		t.Errorf("Result to json failed: %s", err.Error())
	}

	expected, err := json.Marshal(datas.NewElement)
	if err != nil {
		t.Errorf("Element to json failed: %s", err.Error())
	}

	utils.CompareStrings(t, string(jsonResult), string(expected))
}

func TestDaoCreateExistingElement(t *testing.T) {
	_, error := elementDao.Create(a.Db, datas.NewElement)
	if error == nil {
		t.Errorf("Error waited but none provided: %s", error.Error())
	}
}

func TestDaoUpdateElement(t *testing.T) {
	result, error := elementDao.Update(a.Db, datas.UpdatedNewElement)
	if error != nil {
		t.Errorf("update element failed: %s", error.Error())
	}

	jsonResult, err := json.Marshal(result)
	if err != nil {
		t.Errorf("Result to json failed: %s", err.Error())
	}

	expected, err := json.Marshal(datas.UpdatedNewElement)
	if err != nil {
		t.Errorf("Element to json failed: %s", err.Error())
	}

	utils.CompareStrings(t, string(jsonResult), string(expected))
}

func TestDaoDeleteElement(t *testing.T) {
	error := elementDao.Delete(a.Db, datas.DevElem.ID)
	if error != nil {
		t.Errorf("Delete failed: %s", error.Error())
	}

	initDatas()
}
