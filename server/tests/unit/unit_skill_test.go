package unit

import (
	"encoding/json"
	"pokedev/server/dao"
	"pokedev/server/models"
	"pokedev/server/tests/datas"
	"pokedev/server/tests/utils"
	"testing"
)

var skillDao = &dao.SkillDao{}

func TestDAOGetAllSkills(t *testing.T) {
	result, error := skillDao.All(a.Db)
	if error != nil {
		t.Errorf("get all failed: %s", error.Error())
	}

	jsonResult, err := json.Marshal(result)
	if err != nil {
		t.Errorf("Result to json failed: %s", err.Error())
	}

	expected, err := json.Marshal(datas.AllSkills)
	if err != nil {
		t.Errorf("Skill to json failed: %s", err.Error())
	}

	utils.CompareStrings(t, string(jsonResult), string(expected))
}

func TestDaoGetSkillByID(t *testing.T) {
	result, error := skillDao.GetOneByID(a.Db, 1)
	if error != nil {
		t.Errorf("GetOneByID failed: %s", error.Error())
	}

	jsonResult, err := json.Marshal(result)
	if err != nil {
		t.Errorf("Result to json failed: %s", err.Error())
	}

	expected, err := json.Marshal(datas.GoSkill)
	if err != nil {
		t.Errorf("skill to json failed: %s", err.Error())
	}

	utils.CompareStrings(t, string(jsonResult), string(expected))
}

func TestDaoCreateSkill(t *testing.T) {
	var realNewSkill models.Skill
	err := json.Unmarshal(datas.RequestNewSkill, &realNewSkill)
	if err != nil {
		t.Errorf("Unmarshal failed: %s", err.Error())
	}

	result, error := skillDao.Create(a.Db, realNewSkill)
	if error != nil {
		t.Errorf("Create failed: %s", error.Error())
	}

	jsonResult, err := json.Marshal(result)
	if err != nil {
		t.Errorf("Result to json failed: %s", err.Error())
	}

	expected, err := json.Marshal(datas.NewSkill)
	if err != nil {
		t.Errorf("Skill to json failed: %s", err.Error())
	}

	utils.CompareStrings(t, string(jsonResult), string(expected))
}

func TestDaoCreateExistingSkill(t *testing.T) {
	_, error := skillDao.Create(a.Db, datas.NewSkill)
	if error == nil {
		t.Errorf("Error waited but none provided: %s", error.Error())
	}
}

func TestDaoUpdateSkill(t *testing.T) {
	result, error := skillDao.Update(a.Db, datas.UpdatedNewSkill)
	if error != nil {
		t.Errorf("Update failed: %s", error.Error())
	}

	jsonResult, err := json.Marshal(result)
	if err != nil {
		t.Errorf("Result to json failed: %s", err.Error())
	}

	expected, err := json.Marshal(datas.UpdatedNewSkill)
	if err != nil {
		t.Errorf("skill to json failed: %s", err.Error())
	}

	utils.CompareStrings(t, string(jsonResult), string(expected))
}

func TestDaoDeleteSkill(t *testing.T) {
	error := skillDao.Delete(a.Db, datas.GoSkill.ID)
	if error != nil {
		t.Errorf("Delete failed: %s", error.Error())
	}

	initDatas()
}
