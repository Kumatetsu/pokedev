package dao

import (
	"errors"
	"pokedev/server/models"

	"github.com/jinzhu/gorm"
)

//TeamDao gives dao methods for Element
type TeamDao struct {
}

//GetOneByID Get one team By ID
func (*TeamDao) GetOneByID(db *gorm.DB, id int) (models.Team, error) {
	var team models.Team
	err := db.Preload("Members").Preload("Members.Elements").Preload("Members.Skills").First(&team, id).Error
	return team, err
}

//Update update team and return it
func (*TeamDao) Update(db *gorm.DB, team models.Team) (models.Team, error) {
	err := db.Save(&team).Error
	return team, err
}

//Delete will delete team from db
func (*TeamDao) Delete(db *gorm.DB, id int) error {
	var team models.Team

	err := db.Preload("Members").Preload("Members.Elements").Preload("Members.Skills").First(&team, id).Error
	if nil != err {
		return err
	}

	db.Model(&team).Association("Members").Delete(&team.Members)
	err = db.Delete(&team).Error
	return err
}

//Create will create a new team in db
func (*TeamDao) Create(db *gorm.DB, team models.Team) (models.Team, error) {
	if true != db.NewRecord(team) {
		return team, errors.New("team already exists")
	}

	err := db.Create(&team).Error
	return team, err
}

//All will return all team in db
func (*TeamDao) All(db *gorm.DB) ([]models.Team, error) {
	teams := []models.Team{}
	err := db.Preload("Members").Preload("Members.Elements").Preload("Members.Skills").Find(&teams).Error
	return teams, err
}
