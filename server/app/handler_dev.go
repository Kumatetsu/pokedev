package app

import (
	"encoding/json"
	"net/http"
	"pokedev/server/dao"
	"pokedev/server/models"
	"strconv"

	"github.com/gorilla/mux"
)

func (a *App) getAllDevs(w http.ResponseWriter, r *http.Request) {
	devDao := dao.DevDao{}
	devs, err := devDao.All(a.Db)

	if nil != err {
		errorResponse(w, http.StatusBadRequest, err.Error())
		return
	}

	jsonResponse(w, http.StatusOK, devs)
}

func (a *App) getDev(w http.ResponseWriter, r *http.Request) {
	devDao := dao.DevDao{}

	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, err.Error())
		return
	}

	dev, err := devDao.GetOneByID(a.Db, id)
	if nil != err {
		errorResponse(w, http.StatusBadRequest, err.Error())
		return
	}

	jsonResponse(w, http.StatusOK, dev)
}

func (a *App) postDev(w http.ResponseWriter, r *http.Request) {
	devDao := dao.DevDao{}
	decoder := json.NewDecoder(r.Body)

	var newDev models.Dev

	err := decoder.Decode(&newDev)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, err.Error())
		return
	}

	newDev, err = devDao.Create(a.Db, newDev)
	if nil != err {
		errorResponse(w, http.StatusBadRequest, err.Error())
		return
	}

	jsonResponse(w, http.StatusOK, newDev)
}

func (a *App) putDev(w http.ResponseWriter, r *http.Request) {
	devDao := dao.DevDao{}
	decoder := json.NewDecoder(r.Body)

	var updateDev models.Dev

	err := decoder.Decode(&updateDev)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, err.Error())
		return
	}

	updateDev, err = devDao.Update(a.Db, updateDev)
	if nil != err {
		errorResponse(w, http.StatusBadRequest, err.Error())
		return
	}

	jsonResponse(w, http.StatusOK, updateDev)
}

func (a *App) deleteDev(w http.ResponseWriter, r *http.Request) {
	devDao := dao.DevDao{}
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, err.Error())
		return
	}

	err = devDao.Delete(a.Db, id)
	if nil != err {
		errorResponse(w, http.StatusBadRequest, err.Error())
		return
	}

	noContentResponse(w)
}
