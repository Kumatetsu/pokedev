package models

//Team is a container of your best devs
type Team struct {
	ID      int    `json:"id,omitempty"`
	Name    string `json:"name"`
	Members []Dev  `json:"members" gorm:"many2many:dev_has_team"`
}
