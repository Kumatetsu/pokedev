import React, {useState, useEffect} from 'react';
import { Element } from '../models/elements';
import ListElements from '../components/allElementsComponent';
import { IdentifiedType } from '../models/utils';
import { getRequest } from '../utils/httpUtils';

const Component = () => {
    const basicState:IdentifiedType<Element>[] = []
    const [elements, setElements] = useState(basicState);
    const [hasError, setErrors] = useState(false);

    useEffect(() => {
        fetch("http://localhost:8001/elements", getRequest)
        .then(function(response) {
            return response.json();
        })
        .then((elements: IdentifiedType<Element>[]) => {
            setElements(elements)
        })
        .catch((error) => {
            setErrors(error)
        });
    }, []);

    return (
        <div>
            <h2>All Elements</h2>
            {
                0 < elements.length ?
                <ListElements elements={elements}/>:
                <span>No Elements</span>
            }
        </div>

    )
}

export default Component
