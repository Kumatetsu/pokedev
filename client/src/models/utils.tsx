export type IdentifiedType<T> = {
    id: number
} & T
