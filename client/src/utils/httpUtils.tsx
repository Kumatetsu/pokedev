
export const configInit = (body:any) : RequestInit => {
    return {
        method: 'POST',
        mode: 'cors',
        headers: new Headers({
            'Content-Type': 'application/json'
        }),
        body: JSON.stringify(body)
    }
};

export const getRequest: RequestInit= {
    method: 'GET',
    mode:   'cors',
    cache:  'default'
}

export const deleteRequest: RequestInit = {
    method: 'Delete',
    mode:   'cors',
    cache:  'default'
}

export function isValidResponse(response: Response) {
    if (response.ok) {
        return response;
    } else {
        let errorMessage = 'Http request failed';
        let error = new Error(errorMessage);
        throw error;
    }
}
