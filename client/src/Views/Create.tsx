import React, {useState, useEffect} from 'react'
import DevForm from '../components/devFormComponent'
import { IdentifiedType } from '../models/utils';
import { Element } from '../models/elements';
import { Skill } from '../models/skills';

const Component = () => {
    const basicElementState:IdentifiedType<Element>[] = []
    const basicSkillState:IdentifiedType<Skill>[] = []
    const [elements, setElements] = useState(basicElementState);
    const [skills, setSkills] = useState(basicSkillState);
    const [hasError, setErrors] = useState(false);

    async function fetchData(url:string, setFunction:Function) {
        const res = await fetch(url);
        res
        .json()
        .then(res => setFunction(res))
        .catch(err => setErrors(err));
    }

    useEffect(() => {
        fetchData("http://localhost:8001/elements", setElements)
        fetchData("http://localhost:8001/skills", setSkills)
    }, []);

    return (
        <div>
            <h2>Create your dev</h2>
            <DevForm elements={elements} skills={skills}/>
        </div>
    )
}

export default Component
