package unit

import (
	"encoding/json"
	"pokedev/server/dao"
	"pokedev/server/models"
	"pokedev/server/tests/datas"
	"pokedev/server/tests/utils"
	"testing"
)

var devDao = &dao.DevDao{}

func TestDAOGetAllDevs(t *testing.T) {
	result, error := devDao.All(a.Db)
	if error != nil {
		t.Errorf("get all dev failed: %s", error.Error())
	}

	jsonResult, err := json.Marshal(result)
	if err != nil {
		t.Errorf("Result to json failed: %s", err.Error())
	}

	expected, err := json.Marshal(datas.AllDevs)
	if err != nil {
		t.Errorf("Dev to json failed: %s", err.Error())
	}

	utils.CompareStrings(t, string(jsonResult), string(expected))
}

func TestDaoGetDevByIDs(t *testing.T) {
	result, error := devDao.GetOneByID(a.Db, 1)
	if error != nil {
		t.Errorf("get all dev failed: %s", error.Error())
	}

	jsonResult, err := json.Marshal(result)
	if err != nil {
		t.Errorf("Result to json failed: %s", err.Error())
	}

	expected, err := json.Marshal(datas.Jean)
	if err != nil {
		t.Errorf("Dev to json failed: %s", err.Error())
	}

	utils.CompareStrings(t, string(jsonResult), string(expected))
}

func TestDaoCreateDev(t *testing.T) {
	var realNewDev models.Dev
	err := json.Unmarshal(datas.RequestNewDev, &realNewDev)
	if err != nil {
		t.Errorf("Unmarshal failed: %s", err.Error())
	}

	result, error := devDao.Create(a.Db, realNewDev)
	if error != nil {
		t.Errorf("Create failed: %s", error.Error())
	}

	jsonResult, err := json.Marshal(result)
	if err != nil {
		t.Errorf("Result to json failed: %s", err.Error())
	}

	expected, err := json.Marshal(datas.NewDev)
	if err != nil {
		t.Errorf("Dev to json failed: %s", err.Error())
	}

	utils.CompareStrings(t, string(jsonResult), string(expected))
}

func TestDaoCreateExistingDev(t *testing.T) {
	_, error := devDao.Create(a.Db, datas.NewDev)
	if error == nil {
		t.Errorf("Error waited but none provided: %s", error.Error())
	}
}

func TestDaoUpdateDev(t *testing.T) {
	result, error := devDao.Update(a.Db, datas.UpdatedNewDev)
	if error != nil {
		t.Errorf("update failed: %s", error.Error())
	}

	jsonResult, err := json.Marshal(result)
	if err != nil {
		t.Errorf("Result to json failed: %s", err.Error())
	}

	expected, err := json.Marshal(datas.UpdatedNewDev)
	if err != nil {
		t.Errorf("Dev to json failed: %s", err.Error())
	}

	utils.CompareStrings(t, string(jsonResult), string(expected))
}

func TestDaoDeleteDev(t *testing.T) {
	error := devDao.Delete(a.Db, datas.Jean.ID)
	if error != nil {
		t.Errorf("delete failed: %s", error.Error())
	}

	initDatas()
}
