import { Element } from './elements'
import { Skill } from  './skills'

export interface Dev {
    firstname: string,
    lastname: string,
    elements: Element[],
    skills: Skill[]
}
