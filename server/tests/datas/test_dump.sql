# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.2.10-MariaDB)
# Database: test_pokedev
# Generation Time: 2019-07-06 17:10:32 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

# Dump of table dev_has_element
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dev_has_element`;

CREATE TABLE `dev_has_element` (
  `dev_id` int(11) NOT NULL,
  `element_id` int(11) NOT NULL,
  PRIMARY KEY (`dev_id`,`element_id`),
  KEY `element_id` (`element_id`),
  CONSTRAINT `dev_has_element_ibfk_1` FOREIGN KEY (`dev_id`) REFERENCES `devs` (`id`),
  CONSTRAINT `dev_has_element_ibfk_2` FOREIGN KEY (`element_id`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dev_has_element` WRITE;
/*!40000 ALTER TABLE `dev_has_element` DISABLE KEYS */;

INSERT INTO `dev_has_element` (`dev_id`, `element_id`)
VALUES
	(1,1),
	(1,2),
	(2,1),
	(2,2),
	(3,1),
	(3,2),
	(3,3);

/*!40000 ALTER TABLE `dev_has_element` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dev_has_skill
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dev_has_skill`;

CREATE TABLE `dev_has_skill` (
  `dev_id` int(11) NOT NULL,
  `skill_id` int(11) NOT NULL,
  PRIMARY KEY (`dev_id`,`skill_id`),
  KEY `skill_id` (`skill_id`),
  CONSTRAINT `dev_has_skill_ibfk_1` FOREIGN KEY (`dev_id`) REFERENCES `devs` (`id`),
  CONSTRAINT `dev_has_skill_ibfk_2` FOREIGN KEY (`skill_id`) REFERENCES `skills` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dev_has_skill` WRITE;
/*!40000 ALTER TABLE `dev_has_skill` DISABLE KEYS */;

INSERT INTO `dev_has_skill` (`dev_id`, `skill_id`)
VALUES
	(1,1),
	(1,3),
	(2,1),
	(2,2),
	(3,4),
	(3,5);

/*!40000 ALTER TABLE `dev_has_skill` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dev_has_team
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dev_has_team`;

CREATE TABLE `dev_has_team` (
  `team_id` int(11) NOT NULL,
  `dev_id` int(11) NOT NULL,
  PRIMARY KEY (`team_id`,`dev_id`),
  KEY `dev_id` (`dev_id`),
  CONSTRAINT `dev_has_team_ibfk_1` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`) ON DELETE CASCADE,
  CONSTRAINT `dev_has_team_ibfk_2` FOREIGN KEY (`dev_id`) REFERENCES `devs` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dev_has_team` WRITE;
/*!40000 ALTER TABLE `dev_has_team` DISABLE KEYS */;

INSERT INTO `dev_has_team` (`team_id`, `dev_id`)
VALUES
	(1,2),
	(1,3),
	(2,1);

/*!40000 ALTER TABLE `dev_has_team` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table devs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `devs`;

CREATE TABLE `devs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `devs` WRITE;
/*!40000 ALTER TABLE `devs` DISABLE KEYS */;

INSERT INTO `devs` (`id`, `firstname`, `lastname`)
VALUES
	(1,'Jean','Billaud'),
	(2,'Aurel','San'),
	(3,'Chat','Panda');

/*!40000 ALTER TABLE `devs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table elements
# ------------------------------------------------------------

DROP TABLE IF EXISTS `elements`;

CREATE TABLE `elements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `elements` WRITE;
/*!40000 ALTER TABLE `elements` DISABLE KEYS */;

INSERT INTO `elements` (`id`, `name`)
VALUES
	(1,'developper'),
	(2,'devops'),
	(3,'data scientist'),
	(4,'uix');

/*!40000 ALTER TABLE `elements` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table skills
# ------------------------------------------------------------

DROP TABLE IF EXISTS `skills`;

CREATE TABLE `skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `power` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `skills` WRITE;
/*!40000 ALTER TABLE `skills` DISABLE KEYS */;

INSERT INTO `skills` (`id`, `name`, `power`)
VALUES
	(1,'golang',5),
	(2,'angularjs',8),
	(3,'vuejs',6),
	(4,'docker',10),
	(5,'php',8);

/*!40000 ALTER TABLE `skills` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table teams
# ------------------------------------------------------------

DROP TABLE IF EXISTS `teams`;

CREATE TABLE `teams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `teams` WRITE;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;

INSERT INTO `teams` (`id`, `name`)
VALUES
	(1,'chocolate'),
	(2,'vanilla');

/*!40000 ALTER TABLE `teams` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
