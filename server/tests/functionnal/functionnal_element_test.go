package functionnal

import (
	"bytes"
	"encoding/json"
	"net/http"
	"pokedev/server/tests/datas"
	"pokedev/server/tests/utils"
	"testing"
)

func TestGetAllElements(t *testing.T) {
	req, recorder := utils.CreateContext(t, "GET", "/elements", nil)
	a.Router.ServeHTTP(recorder, req)
	if recorder.Code != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", recorder.Code, http.StatusOK)
	}

	expected, err := json.Marshal(datas.AllElements)
	if err != nil {
		t.Errorf("array to json failed: %s", err.Error())
	}

	utils.CompareStrings(t, recorder.Body.String(), string(expected))
}

func TestGetElementById(t *testing.T) {
	req, recorder := utils.CreateContext(t, "GET", "/elements/1", nil)
	a.Router.ServeHTTP(recorder, req)
	if recorder.Code != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", recorder.Code, http.StatusOK)
	}

	expected, err := json.Marshal(datas.DevElem)
	if err != nil {
		t.Errorf("Dev to json failed: %s", err.Error())
	}

	utils.CompareStrings(t, recorder.Body.String(), string(expected))
}

func TestCreateElement(t *testing.T) {
	req, recorder := utils.CreateContext(t, "POST", "/elements", bytes.NewBuffer(datas.RequestNewElement))
	req.Header.Set("Content-Type", "application/json")
	a.Router.ServeHTTP(recorder, req)
	if recorder.Code != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", recorder.Code, http.StatusOK)
	}

	expected, err := json.Marshal(datas.NewElement)
	if err != nil {
		t.Errorf("Dev to json failed: %s", err.Error())
	}

	utils.CompareStrings(t, recorder.Body.String(), string(expected))
}

func TestUpdateElement(t *testing.T) {
	req, recorder := utils.CreateContext(t, "PUT", "/elements/4", bytes.NewBuffer(datas.RequestUpdateNewElement))
	a.Router.ServeHTTP(recorder, req)
	if recorder.Code != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", recorder.Code, http.StatusOK)
	}

	expected, err := json.Marshal(datas.UpdatedNewElement)
	if err != nil {
		t.Errorf("Dev to json failed: %s", err.Error())
	}

	utils.CompareStrings(t, recorder.Body.String(), string(expected))
}

func TestDeleteElement(t *testing.T) {
	req, recorder := utils.CreateContext(t, "DELETE", "/elements/2", nil)
	a.Router.ServeHTTP(recorder, req)
	if recorder.Code != http.StatusNoContent {
		t.Errorf("handler returned wrong status code: got %v want %v", recorder.Code, http.StatusNoContent)
	}

	initDatas()
}
