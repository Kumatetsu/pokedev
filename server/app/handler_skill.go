package app

import (
	"encoding/json"
	"fmt"
	"net/http"
	"pokedev/server/dao"
	"pokedev/server/models"
	"strconv"

	"github.com/gorilla/mux"
)

func (a *App) getAllSkills(w http.ResponseWriter, r *http.Request) {
	skillDao := dao.SkillDao{}
	skills, err := skillDao.All(a.Db)

	if nil != err {
		errorResponse(w, http.StatusBadRequest, err.Error())
		return
	}

	jsonResponse(w, http.StatusOK, skills)
}

func (a *App) getSkill(w http.ResponseWriter, r *http.Request) {
	skillDao := dao.SkillDao{}

	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		w.WriteHeader(400)
		fmt.Fprintf(w, "%s", err.Error())
		return
	}

	skill, err := skillDao.GetOneByID(a.Db, id)
	if nil != err {
		errorResponse(w, http.StatusBadRequest, err.Error())
		return
	}

	jsonResponse(w, http.StatusOK, skill)
}

func (a *App) postSkill(w http.ResponseWriter, r *http.Request) {
	skillDao := dao.SkillDao{}
	decoder := json.NewDecoder(r.Body)

	var newSkill models.Skill

	err := decoder.Decode(&newSkill)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, err.Error())
		return
	}

	newSkill, err = skillDao.Create(a.Db, newSkill)
	if nil != err {
		errorResponse(w, http.StatusBadRequest, err.Error())
		return
	}

	jsonResponse(w, http.StatusOK, newSkill)
}

func (a *App) putSkill(w http.ResponseWriter, r *http.Request) {
	skillDao := dao.SkillDao{}
	decoder := json.NewDecoder(r.Body)

	var updateSkill models.Skill

	err := decoder.Decode(&updateSkill)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, err.Error())
		return
	}

	updateSkill, err = skillDao.Update(a.Db, updateSkill)
	if nil != err {
		errorResponse(w, http.StatusBadRequest, err.Error())
		return
	}

	jsonResponse(w, http.StatusOK, updateSkill)
}

func (a *App) deleteSkill(w http.ResponseWriter, r *http.Request) {
	skill := dao.SkillDao{}
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, err.Error())
		return
	}

	err = skill.Delete(a.Db, id)
	if nil != err {
		errorResponse(w, http.StatusBadRequest, err.Error())
		return
	}

	noContentResponse(w)
}
