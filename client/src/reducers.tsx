import { combineReducers } from 'redux'
import devReducer from './store/dev/reducer'

export default combineReducers({
    devReducer
})
