package models

//Dev are the characters of the game
type Dev struct {
	ID        int       `json:"id,omitempty"`
	Firstname string    `json:"firstname"`
	Lastname  string    `json:"lastname"`
	Elements  []Element `json:"elements" gorm:"many2many:dev_has_element"`
	Skills    []Skill   `json:"skills" gorm:"many2many:dev_has_skill"`
}
