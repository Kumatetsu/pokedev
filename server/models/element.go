package models

//Element is like your expertise field
type Element struct {
	ID   int    `json:"id,omitempty"`
	Name string `json:"name"`
}
