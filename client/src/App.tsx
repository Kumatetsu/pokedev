import React from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import {Toolbar, Button, AppBar} from '@material-ui/core';
import './App.css';
import Home from './Views/Home'
import Pokedev from './Views/Pokedev'
import Create from './Views/Create'
import Elements from './Views/Elements'
import Skills from './Views/Skills'

const App: React.FC = () => {
    return (
        <Router>
            <div>
                <AppBar position="fixed">
                    <Toolbar>
                        <Button>
                            <Link to="/">Home</Link>
                        </Button>
                        <Button>
                            <Link to="/pokedev">Pokedevs</Link>
                        </Button>
                        <Button>
                            <Link to="/elements">Elements</Link>
                        </Button>
                        <Button>
                            <Link to="/skills">Skills</Link>
                        </Button>
                    </Toolbar>
                </AppBar>
                <div className="Container">
                    <Route path="/" exact render={() => (
                        <div className="App">
                            <Home />
                        </div>
                    )}/>
                    <Route exact={true} path='/pokedev' render={() => (
                        <div className="App">
                            <Pokedev />
                        </div>
                    )}/>
                    <Route exact={true} path='/elements' render={() => (
                        <div className="App">
                            <Elements />
                        </div>
                    )}/>
                    <Route exact={true} path='/skills' render={() => (
                        <div className="App">
                            <Skills />
                        </div>
                    )}/>
                    <Route exact={true} path='/create' render={() => (
                        <div className="App">
                            <Create />
                        </div>
                    )}/>
                </div>
            </div>
        </Router>
    );
}

export default App;
