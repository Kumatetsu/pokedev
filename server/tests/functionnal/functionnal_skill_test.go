package functionnal

import (
	"bytes"
	"encoding/json"
	"net/http"
	"pokedev/server/tests/datas"
	"pokedev/server/tests/utils"
	"testing"
)

func TestGetAllSkills(t *testing.T) {
	req, recorder := utils.CreateContext(t, "GET", "/skills", nil)
	a.Router.ServeHTTP(recorder, req)
	if recorder.Code != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", recorder.Code, http.StatusOK)
	}

	expected, err := json.Marshal(datas.AllSkills)
	if err != nil {
		t.Errorf("array to json failed: %s", err.Error())
	}

	utils.CompareStrings(t, recorder.Body.String(), string(expected))
}

func TestGetSkillById(t *testing.T) {
	req, recorder := utils.CreateContext(t, "GET", "/skills/1", nil)
	a.Router.ServeHTTP(recorder, req)
	if recorder.Code != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", recorder.Code, http.StatusOK)
	}

	expected, err := json.Marshal(datas.GoSkill)
	if err != nil {
		t.Errorf("Dev to json failed: %s", err.Error())
	}

	utils.CompareStrings(t, recorder.Body.String(), string(expected))
}

func TestCreateSkill(t *testing.T) {
	req, recorder := utils.CreateContext(t, "POST", "/skills", bytes.NewBuffer(datas.RequestNewSkill))
	req.Header.Set("Content-Type", "application/json")
	a.Router.ServeHTTP(recorder, req)
	if recorder.Code != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", recorder.Code, http.StatusOK)
	}

	expected, err := json.Marshal(datas.NewSkill)
	if err != nil {
		t.Errorf("Dev to json failed: %s", err.Error())
	}

	utils.CompareStrings(t, recorder.Body.String(), string(expected))
}

func TestUpdateSkill(t *testing.T) {
	req, recorder := utils.CreateContext(t, "PUT", "/skills/6", bytes.NewBuffer(datas.RequestUpdateNewSkill))
	a.Router.ServeHTTP(recorder, req)
	if recorder.Code != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", recorder.Code, http.StatusOK)
	}

	expected, err := json.Marshal(datas.UpdatedNewSkill)
	if err != nil {
		t.Errorf("Dev to json failed: %s", err.Error())
	}

	utils.CompareStrings(t, recorder.Body.String(), string(expected))
}

func TestDeleteSkill(t *testing.T) {
	req, recorder := utils.CreateContext(t, "DELETE", "/skills/2", nil)
	a.Router.ServeHTTP(recorder, req)
	if recorder.Code != http.StatusNoContent {
		t.Errorf("handler returned wrong status code: got %v want %v", recorder.Code, http.StatusNoContent)
	}

	initDatas()
}
