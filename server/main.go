package main

import (
	"net/http"
	"os"
	"pokedev/server/app"

	"github.com/gorilla/handlers"

	//db driver library import
	_ "github.com/go-sql-driver/mysql"
)

const user = "root"
const password = ""
const db = "pokedev"

func main() {
	a := app.App{}
	a.Initialize(os.Getenv("MYSQL_USER"), os.Getenv("MYSQL_PASSWORD"), os.Getenv("MYSQL_DATABASE"))
	defer a.Db.Close()

	headersOk := handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization"})
	originsOk := handlers.AllowedOrigins([]string{"*"})
	methodsOk := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS"})

	http.ListenAndServe(":8001", handlers.CORS(originsOk, headersOk, methodsOk)(a.Router))
}
