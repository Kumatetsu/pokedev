package functionnal

import (
	"net/http"
	"pokedev/server/tests/utils"
	"testing"
)

func TestGetHome(t *testing.T) {
	req, recorder := utils.CreateContext(t, "GET", "/", nil)
	a.Router.ServeHTTP(recorder, req)
	if recorder.Code != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", recorder.Code, http.StatusOK)
	}

	expected := `{"message": "Hello world !"}`

	utils.CompareStrings(t, recorder.Body.String(), expected)
}
