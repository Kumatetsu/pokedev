package datas

import "pokedev/server/models"

//elements datas
var DevElem = models.Element{ID: 1, Name: "developper"}
var DevopsElem = models.Element{ID: 2, Name: "devops"}
var DataElem = models.Element{ID: 3, Name: "data scientist"}
var UixElem = models.Element{ID: 4, Name: "uix"}

var NewElement = models.Element{ID: 5, Name: "NeoDevelopper"}
var UpdatedNewElement = models.Element{ID: 5, Name: "TrinityDevelopper"}

var ToDeleteElem = models.Element{Name: "toDelete"}

//skills datas
var GoSkill = models.Skill{ID: 1, Name: "golang", Power: 5}
var AngularJsSkill = models.Skill{ID: 2, Name: "angularjs", Power: 8}
var VueJsSkill = models.Skill{ID: 3, Name: "vuejs", Power: 6}
var DockerSkill = models.Skill{ID: 4, Name: "docker", Power: 10}
var PhpSkill = models.Skill{ID: 5, Name: "php", Power: 8}

var NewSkill = models.Skill{ID: 6, Name: "python", Power: 7}
var UpdatedNewSkill = models.Skill{ID: 6, Name: "snake", Power: 7}

var ToDeleteSkill = models.Skill{Name: "", Power: 0}

//Devs datas
var Jean = models.Dev{
	ID:        1,
	Firstname: "Jean",
	Lastname:  "Billaud",
	Elements:  []models.Element{DevElem, DevopsElem},
	Skills:    []models.Skill{GoSkill, VueJsSkill},
}

var Aurel = models.Dev{
	ID:        2,
	Firstname: "Aurel",
	Lastname:  "San",
	Elements:  []models.Element{DevElem, DevopsElem},
	Skills:    []models.Skill{GoSkill, AngularJsSkill},
}

var Panda = models.Dev{
	ID:        3,
	Firstname: "Chat",
	Lastname:  "Panda",
	Elements:  []models.Element{DevElem, DevopsElem, DataElem},
	Skills:    []models.Skill{DockerSkill, PhpSkill},
}

var NewDev = models.Dev{
	ID:        4,
	Firstname: "Petit",
	Lastname:  "Nouveau",
	Elements:  []models.Element{DevElem},
	Skills:    []models.Skill{PhpSkill},
}

var UpdatedNewDev = models.Dev{
	ID:        4,
	Firstname: "Petit",
	Lastname:  "Nouveau",
	Elements:  []models.Element{DevElem},
	Skills:    []models.Skill{DockerSkill, PhpSkill},
}

var ToDeleteDev = models.Dev{
	Firstname: "Petit",
	Lastname:  "Nouveau",
}

//Team datas
var ChocolateTeam = models.Team{ID: 1, Name: "chocolate", Members: []models.Dev{Aurel, Panda}}

var VanillaTeam = models.Team{ID: 2, Name: "vanilla", Members: []models.Dev{Jean}}

var NewTeam = models.Team{ID: 3, Name: "caramel", Members: []models.Dev{}}

var UpdatedNewTeam = models.Team{ID: 3, Name: "caramel", Members: []models.Dev{Jean}}

var ToDeleteTeam = models.Team{Name: "caramel"}

//All results
var AllDevs = []models.Dev{
	Jean,
	Aurel,
	Panda,
}

var AllElements = []models.Element{
	DevElem,
	DevopsElem,
	DataElem,
	UixElem,
}

var AllSkills = []models.Skill{
	GoSkill,
	AngularJsSkill,
	VueJsSkill,
	DockerSkill,
	PhpSkill,
}

var AllTeams = []models.Team{
	ChocolateTeam,
	VanillaTeam,
}
