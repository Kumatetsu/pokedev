package app

import (
	"encoding/json"
	"net/http"
	"pokedev/server/dao"
	"pokedev/server/models"
	"strconv"

	"github.com/gorilla/mux"
)

func (a *App) getAllElements(w http.ResponseWriter, r *http.Request) {
	elementDao := dao.ElementDao{}
	elements, err := elementDao.All(a.Db)

	if nil != err {
		errorResponse(w, http.StatusBadRequest, err.Error())
		return
	}

	jsonResponse(w, http.StatusOK, elements)
}

func (a *App) getElement(w http.ResponseWriter, r *http.Request) {
	elementDao := dao.ElementDao{}

	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, err.Error())
		return
	}

	element, err := elementDao.GetOneByID(a.Db, id)
	if nil != err {
		errorResponse(w, http.StatusBadRequest, err.Error())
		return
	}

	jsonResponse(w, http.StatusOK, element)
}

func (a *App) postElement(w http.ResponseWriter, r *http.Request) {
	elementDao := dao.ElementDao{}
	decoder := json.NewDecoder(r.Body)

	var newElement models.Element

	err := decoder.Decode(&newElement)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, err.Error())
		return
	}

	newElement, err = elementDao.Create(a.Db, newElement)
	if nil != err {
		errorResponse(w, http.StatusBadRequest, err.Error())
		return
	}

	jsonResponse(w, http.StatusOK, newElement)
}

func (a *App) putElement(w http.ResponseWriter, r *http.Request) {
	elementDao := dao.ElementDao{}
	decoder := json.NewDecoder(r.Body)

	var updateElement models.Element

	err := decoder.Decode(&updateElement)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, err.Error())
		return
	}

	updateElement, err = elementDao.Update(a.Db, updateElement)
	if nil != err {
		errorResponse(w, http.StatusBadRequest, err.Error())
		return
	}

	jsonResponse(w, http.StatusOK, updateElement)
}

func (a *App) deleteElement(w http.ResponseWriter, r *http.Request) {
	elementDao := dao.ElementDao{}
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, err.Error())
		return
	}

	err = elementDao.Delete(a.Db, id)
	if nil != err {
		errorResponse(w, http.StatusBadRequest, err.Error())
		return
	}

	noContentResponse(w)
}
