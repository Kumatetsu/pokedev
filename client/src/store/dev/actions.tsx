import {DevActionTypes, DELETE_DEV, SET_DEVS, DevState, ADD_DEV} from './types'
import { AnyAction } from 'redux';
import { IdentifiedType } from '../../models/utils';
import {Dev} from '../../models/devs';
import { ThunkAction, ThunkDispatch } from 'redux-thunk'
import { getRequest, deleteRequest, isValidResponse } from '../../utils/httpUtils';

export function addDev(dev: Dev): DevActionTypes {
    return {
        type: ADD_DEV,
        payload: dev
    }
}

export function SetDevs(Devs: Dev[]): DevActionTypes {
    return {
        type: SET_DEVS,
        payload: Devs
    }
}

export function DelDev(dev: IdentifiedType<Dev>): DevActionTypes {
    return {
        type: DELETE_DEV,
        payload: dev
    }
}

export function loadDevsAsync() : ThunkAction<Promise<void>, {}, {}, AnyAction>  {
    return async (dispatch: ThunkDispatch<DevState,{}, DevActionTypes>) => {
        fetch("http://localhost:8001/devs", getRequest)
        .then((response: Response) =>  isValidResponse(response).json())
        .then((devs: Dev[]) => {
            dispatch(SetDevs(devs));
        })
        .catch((error) => {
            console.error(error)
        });
    }
}

export function deleteDevAsync(dev: IdentifiedType<Dev>): ThunkAction<Promise<void>, {}, {}, AnyAction> {
    return async (dispatch: ThunkDispatch<DevState,{}, DevActionTypes>) => {
        fetch(`http://localhost:8001/devs/${dev.id}`, deleteRequest)
        .then((response: Response) => isValidResponse(response))
        .then( () => {
            dispatch(DelDev(dev));
        })
        .catch((error) => {
            console.error(error)
        });
    }
}
