package app

import "net/http"

//Road is a struct to define a Road object
type Road struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

func (a *App) getHomeRouting() []*Road {
	return []*Road{
		{
			Name:        "GET home",
			Method:      "GET",
			Pattern:     "/",
			HandlerFunc: a.getHome,
		},
	}
}

func (a *App) getDevRouting() []*Road {
	return []*Road{
		{
			Name:        "GET Devs",
			Method:      "GET",
			Pattern:     "/devs",
			HandlerFunc: a.getAllDevs,
		},
		{
			Name:        "GET Dev",
			Method:      "GET",
			Pattern:     "/devs/{id}",
			HandlerFunc: a.getDev,
		},
		{
			Name:        "POST Devs",
			Method:      "POST",
			Pattern:     "/devs",
			HandlerFunc: a.postDev,
		},
		{
			Name:        "PUT Dev",
			Method:      "PUT",
			Pattern:     "/devs/{id}",
			HandlerFunc: a.putDev,
		},
		{
			Name:        "DELETE Devs",
			Method:      "DELETE",
			Pattern:     "/devs/{id}",
			HandlerFunc: a.deleteDev,
		},
	}
}

func (a *App) getSkillRouting() []*Road {
	return []*Road{
		{
			Name:        "GET Skills",
			Method:      "GET",
			Pattern:     "/skills",
			HandlerFunc: a.getAllSkills,
		},
		{
			Name:        "GET Skill",
			Method:      "GET",
			Pattern:     "/skills/{id}",
			HandlerFunc: a.getSkill,
		},
		{
			Name:        "POST Skills",
			Method:      "POST",
			Pattern:     "/skills",
			HandlerFunc: a.postSkill,
		},
		{
			Name:        "PUT Skill",
			Method:      "PUT",
			Pattern:     "/skills/{id}",
			HandlerFunc: a.putSkill,
		},
		{
			Name:        "DELETE Skills",
			Method:      "DELETE",
			Pattern:     "/skills/{id}",
			HandlerFunc: a.deleteSkill,
		},
	}
}

func (a *App) getElementRouting() []*Road {
	return []*Road{
		{
			Name:        "GET Elements",
			Method:      "GET",
			Pattern:     "/elements",
			HandlerFunc: a.getAllElements,
		},
		{
			Name:        "GET Element",
			Method:      "GET",
			Pattern:     "/elements/{id}",
			HandlerFunc: a.getElement,
		},
		{
			Name:        "POST Elements",
			Method:      "POST",
			Pattern:     "/elements",
			HandlerFunc: a.postElement,
		},
		{
			Name:        "PUT Element",
			Method:      "PUT",
			Pattern:     "/elements/{id}",
			HandlerFunc: a.putElement,
		},
		{
			Name:        "DELETE Elements",
			Method:      "DELETE",
			Pattern:     "/elements/{id}",
			HandlerFunc: a.deleteElement,
		},
	}
}

func (a *App) getTeamRouting() []*Road {
	return []*Road{
		{
			Name:        "GET Teams",
			Method:      "GET",
			Pattern:     "/teams",
			HandlerFunc: a.getAllTeams,
		},
		{
			Name:        "GET Team",
			Method:      "GET",
			Pattern:     "/teams/{id}",
			HandlerFunc: a.getTeam,
		},
		{
			Name:        "POST Teams",
			Method:      "POST",
			Pattern:     "/teams",
			HandlerFunc: a.postTeam,
		},
		{
			Name:        "PUT Team",
			Method:      "PUT",
			Pattern:     "/teams/{id}",
			HandlerFunc: a.putTeam,
		},
		{
			Name:        "DELETE Teams",
			Method:      "DELETE",
			Pattern:     "/teams/{id}",
			HandlerFunc: a.deleteTeam,
		},
	}
}
