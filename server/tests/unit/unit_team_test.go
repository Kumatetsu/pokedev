package unit

import (
	"encoding/json"
	"pokedev/server/dao"
	"pokedev/server/models"
	"pokedev/server/tests/datas"
	"pokedev/server/tests/utils"
	"testing"
)

var teamDao = &dao.TeamDao{}

func TestDAOGetAllTeams(t *testing.T) {
	result, error := teamDao.All(a.Db)
	if error != nil {
		t.Errorf("get all failed: %s", error.Error())
	}

	jsonResult, err := json.Marshal(result)
	if err != nil {
		t.Errorf("Result to json failed: %s", err.Error())
	}

	expected, err := json.Marshal(datas.AllTeams)
	if err != nil {
		t.Errorf("Team to json failed: %s", err.Error())
	}

	utils.CompareStrings(t, string(jsonResult), string(expected))
}

func TestDaoGetTeamByID(t *testing.T) {
	result, error := teamDao.GetOneByID(a.Db, 1)
	if error != nil {
		t.Errorf("GetOneByID failed: %s", error.Error())
	}

	jsonResult, err := json.Marshal(result)
	if err != nil {
		t.Errorf("Result to json failed: %s", err.Error())
	}

	expected, err := json.Marshal(datas.ChocolateTeam)
	if err != nil {
		t.Errorf("Team to json failed: %s", err.Error())
	}

	utils.CompareStrings(t, string(jsonResult), string(expected))
}

func TestDaoCreateTeam(t *testing.T) {
	var realNewTeam models.Team
	err := json.Unmarshal(datas.RequestNewTeam, &realNewTeam)
	if err != nil {
		t.Errorf("Unmarshal failed: %s", err.Error())
	}

	result, error := teamDao.Create(a.Db, realNewTeam)
	if error != nil {
		t.Errorf("Create failed: %s", error.Error())
	}

	jsonResult, err := json.Marshal(result)
	if err != nil {
		t.Errorf("Result to json failed: %s", err.Error())
	}

	expected, err := json.Marshal(datas.NewTeam)
	if err != nil {
		t.Errorf("Team to json failed: %s", err.Error())
	}

	utils.CompareStrings(t, string(jsonResult), string(expected))
}

func TestDaoCreateExistingTeam(t *testing.T) {
	_, error := teamDao.Create(a.Db, datas.NewTeam)
	if error == nil {
		t.Errorf("Error waited but none provided: %s", error.Error())
	}
}

func TestDaoUpdateTeam(t *testing.T) {
	result, error := teamDao.Update(a.Db, datas.UpdatedNewTeam)
	if error != nil {
		t.Errorf("Update failed: %s", error.Error())
	}

	jsonResult, err := json.Marshal(result)
	if err != nil {
		t.Errorf("Result to json failed: %s", err.Error())
	}

	expected, err := json.Marshal(datas.UpdatedNewTeam)
	if err != nil {
		t.Errorf("skill to json failed: %s", err.Error())
	}

	utils.CompareStrings(t, string(jsonResult), string(expected))
}

func TestDaoDeleteTeam(t *testing.T) {
	error := teamDao.Delete(a.Db, datas.ChocolateTeam.ID)
	if error != nil {
		t.Errorf("Delete failed: %s", error.Error())
	}

	initDatas()
}
