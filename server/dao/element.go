package dao

import (
	"errors"
	"pokedev/server/models"

	"github.com/jinzhu/gorm"
)

//ElementDao gives dao methods for Element
type ElementDao struct {
}

//GetOneByID Get one element By ID
func (*ElementDao) GetOneByID(db *gorm.DB, id int) (models.Element, error) {
	var elem models.Element
	err := db.First(&elem, id).Error
	return elem, err
}

//Update update element and return it
func (*ElementDao) Update(db *gorm.DB, element models.Element) (models.Element, error) {
	err := db.Save(&element).Error
	return element, err
}

//Delete will delete element from db
func (*ElementDao) Delete(db *gorm.DB, id int) error {
	var element models.Element

	err := db.First(&element, id).Error
	if nil != err {
		return err
	}

	err = db.Delete(&element).Error
	return err
}

//Create will create a new element in db
func (*ElementDao) Create(db *gorm.DB, element models.Element) (models.Element, error) {
	if true != db.NewRecord(element) {
		return element, errors.New("element already exists")
	}

	err := db.Create(&element).Error
	return element, err
}

//All will return all element in db
func (*ElementDao) All(db *gorm.DB) ([]models.Element, error) {
	elements := []models.Element{}
	err := db.Find(&elements).Error
	return elements, err
}
