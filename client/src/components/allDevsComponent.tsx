import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Dev } from '../models/devs';
import Button from '@material-ui/core/Button';
import { IdentifiedType } from '../models/utils';

interface props {
    devs: IdentifiedType<Dev>[]
    deleteAction: (dev: IdentifiedType<Dev>) => Promise<void>
}

const component = (props:props) => {
    return (
        <div>
            <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>Firstname</TableCell>
                    <TableCell>Lastname</TableCell>
                    <TableCell>Actions</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {props.devs.map( (dev:IdentifiedType<Dev>) => (
                    <TableRow key={dev.id}>
                      <TableCell component="th" scope="row">
                        {dev.firstname}
                      </TableCell>
                      <TableCell>{dev.lastname}</TableCell>
                      <TableCell>
                          <Button  color="primary" >Modify</Button>
                          <Button  color="secondary"  onClick={ () => props.deleteAction(dev)}>Delete</Button>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
            </Table>
        </div>
    )
}

export default component
