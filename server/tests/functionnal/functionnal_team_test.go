package functionnal

import (
	"bytes"
	"encoding/json"
	"net/http"
	"pokedev/server/tests/datas"
	"pokedev/server/tests/utils"
	"testing"
)

func TestGetAllTeams(t *testing.T) {
	req, recorder := utils.CreateContext(t, "GET", "/teams", nil)
	a.Router.ServeHTTP(recorder, req)
	if recorder.Code != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", recorder.Code, http.StatusOK)
	}

	expected, err := json.Marshal(datas.AllTeams)
	if err != nil {
		t.Errorf("array to json failed: %s", err.Error())
	}

	utils.CompareStrings(t, recorder.Body.String(), string(expected))
}

func TestGetTeamById(t *testing.T) {
	req, recorder := utils.CreateContext(t, "GET", "/teams/1", nil)
	a.Router.ServeHTTP(recorder, req)
	if recorder.Code != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", recorder.Code, http.StatusOK)
	}

	expected, err := json.Marshal(datas.ChocolateTeam)
	if err != nil {
		t.Errorf("team to json failed: %s", err.Error())
	}

	utils.CompareStrings(t, recorder.Body.String(), string(expected))
}

func TestCreateTeam(t *testing.T) {
	req, recorder := utils.CreateContext(t, "POST", "/teams", bytes.NewBuffer(datas.RequestNewTeam))
	req.Header.Set("Content-Type", "application/json")
	a.Router.ServeHTTP(recorder, req)
	if recorder.Code != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", recorder.Code, http.StatusOK)
	}

	expected, err := json.Marshal(datas.NewTeam)
	if err != nil {
		t.Errorf("Dev to json failed: %s", err.Error())
	}

	utils.CompareStrings(t, recorder.Body.String(), string(expected))
}

func TestUpdateTeam(t *testing.T) {
	req, recorder := utils.CreateContext(t, "PUT", "/teams/3", bytes.NewBuffer(datas.RequestUpdateNewTeam))
	a.Router.ServeHTTP(recorder, req)
	if recorder.Code != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", recorder.Code, http.StatusOK)
	}

	expected, err := json.Marshal(datas.UpdatedNewTeam)
	if err != nil {
		t.Errorf("Dev to json failed: %s", err.Error())
	}

	utils.CompareStrings(t, recorder.Body.String(), string(expected))
}

func TestDeleteTeam(t *testing.T) {
	req, recorder := utils.CreateContext(t, "DELETE", "/teams/1", nil)
	a.Router.ServeHTTP(recorder, req)
	if recorder.Code != http.StatusNoContent {
		t.Errorf("handler returned wrong status code: got %v want %v", recorder.Code, http.StatusNoContent)
	}

	initDatas()
}
