package dao

import (
	"errors"
	"pokedev/server/models"

	"github.com/jinzhu/gorm"
)

//DevDao is Dao for dev
type DevDao struct {
}

//GetOneByID Get one dev By ID
func (*DevDao) GetOneByID(db *gorm.DB, id int) (models.Dev, error) {
	var dev models.Dev
	err := db.Set("gorm:auto_preload", true).First(&dev, id).Error
	return dev, err
}

//Update update dev and return it
func (*DevDao) Update(db *gorm.DB, dev models.Dev) (models.Dev, error) {
	err := db.Set("gorm:association_autoupdate", true).Save(&dev).Error
	return dev, err
}

//Delete will delete dev from db
func (*DevDao) Delete(db *gorm.DB, id int) error {
	var dev models.Dev

	err := db.Set("gorm:auto_preload", true).First(&dev, id).Error
	if nil != err {
		return err
	}

	db.Model(&dev).Where("id = ?", id).Association("Skills").Delete(&dev.Skills)
	db.Model(&dev).Where("id = ?", id).Association("Elements").Delete(&dev.Elements)
	err = db.Delete(&dev).Error
	return err
}

//Create will create a new dev in db
func (*DevDao) Create(db *gorm.DB, dev models.Dev) (models.Dev, error) {
	if true != db.NewRecord(dev) {
		return dev, errors.New("dev already exists")
	}

	err := db.Create(&dev).Error
	return dev, err
}

//All will return all dev in db
func (*DevDao) All(db *gorm.DB) ([]models.Dev, error) {
	devs := []models.Dev{}
	err := db.Preload("Skills").Preload("Elements").Find(&devs).Error
	return devs, err
}
