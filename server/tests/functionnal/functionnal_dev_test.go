package functionnal

import (
	"bytes"
	"encoding/json"
	"net/http"
	"pokedev/server/tests/datas"
	"pokedev/server/tests/utils"
	"testing"
)

func TestGetAllDevs(t *testing.T) {
	req, recorder := utils.CreateContext(t, "GET", "/devs", nil)
	a.Router.ServeHTTP(recorder, req)
	if recorder.Code != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", recorder.Code, http.StatusOK)
	}

	expected, err := json.Marshal(datas.AllDevs)
	if err != nil {
		t.Errorf("array to json failed: %s", err.Error())
	}

	utils.CompareStrings(t, recorder.Body.String(), string(expected))
}

func TestGetDevById(t *testing.T) {
	req, recorder := utils.CreateContext(t, "GET", "/devs/1", nil)
	a.Router.ServeHTTP(recorder, req)
	if recorder.Code != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", recorder.Code, http.StatusOK)
	}

	expected, err := json.Marshal(datas.Jean)
	if err != nil {
		t.Errorf("Dev to json failed: %s", err.Error())
	}

	utils.CompareStrings(t, recorder.Body.String(), string(expected))
}

func TestCreateDev(t *testing.T) {
	req, recorder := utils.CreateContext(t, "POST", "/devs", bytes.NewBuffer(datas.RequestNewDev))
	req.Header.Set("Content-Type", "application/json")
	a.Router.ServeHTTP(recorder, req)
	if recorder.Code != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", recorder.Code, http.StatusOK)
	}

	expected, err := json.Marshal(datas.NewDev)
	if err != nil {
		t.Errorf("Dev to json failed: %s", err.Error())
	}

	utils.CompareStrings(t, recorder.Body.String(), string(expected))
}

func TestUpdateDev(t *testing.T) {
	req, recorder := utils.CreateContext(t, "PUT", "/devs/4", bytes.NewBuffer(datas.RequestUpdateNewDev))
	a.Router.ServeHTTP(recorder, req)
	if recorder.Code != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", recorder.Code, http.StatusOK)
	}

	expected, err := json.Marshal(datas.UpdatedNewDev)
	if err != nil {
		t.Errorf("Dev to json failed: %s", err.Error())
	}

	utils.CompareStrings(t, recorder.Body.String(), string(expected))
}

func TestDeleteDev(t *testing.T) {
	req, recorder := utils.CreateContext(t, "DELETE", "/devs/3", nil)
	a.Router.ServeHTTP(recorder, req)
	if recorder.Code != http.StatusNoContent {
		t.Errorf("handler returned wrong status code: got %v want %v", recorder.Code, http.StatusNoContent)
	}

	initDatas()
}
