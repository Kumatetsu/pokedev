import React from 'react';
import { IdentifiedType } from '../models/utils';
import { Element } from '../models/elements';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

interface props {
    elements: IdentifiedType<Element>[]
}

const Component = (props:props) => {
    return (
        <List component="nav" aria-label="Main mailbox folders" >
            {props.elements.map( (element:IdentifiedType<Element>) => (
                <ListItem key={element.id} button alignItems="center">
                    <ListItemText primary={element.name}/>
                </ListItem>
            ))}
        </List>
    )
}

export default Component
