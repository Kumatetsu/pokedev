package app

import (
	"github.com/gorilla/mux"
)

//RoadGetter is a function that return
//a Slice of Road struct type
type roadGetter func() []*Road

func (a *App) initRouter() *mux.Router {
	router := mux.NewRouter()
	for _, roadGetter := range []roadGetter{
		a.getHomeRouting,
		a.getDevRouting,
		a.getSkillRouting,
		a.getElementRouting,
		a.getTeamRouting,
	} {
		for _, road := range roadGetter() {
			router.
				Methods(road.Method).
				Path(road.Pattern).
				Name(road.Name).
				Handler(road.HandlerFunc)
		}
	}

	return router
}
