package unit

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"pokedev/server/app"
	"strings"
	"testing"
)

var a app.App

const dumpPath = "src/pokedev/server/tests/datas/test_dump.sql"
const testUser = "root"
const testPassword = ""
const testDb = "test_pokedev"

func TestMain(m *testing.M) {
	fmt.Printf("TestMain\n")
	a = app.App{}
	a.Initialize(os.Getenv("MYSQL_USER"), os.Getenv("MYSQL_PASSWORD"), os.Getenv("MYSQL_DATABASE"))
	initDatas()
	code := m.Run()

	os.Exit(code)
}

//initDatas, load the sql script
//and execute it
func initDatas() {
	gopath, err := filepath.Abs(os.Getenv("GOPATH"))
	if err != nil {
		log.Fatal(err)
	}

	path := filepath.Join(gopath, dumpPath)

	content, ioErr := ioutil.ReadFile(path)
	if ioErr != nil {
		log.Fatal(ioErr)
	}

	queries := strings.SplitAfter(string(content), ";")

	for _, query := range queries {
		if strings.TrimSpace(query) != "" {
			_, err = a.Db.DB().Exec(query)
			if err != nil {
				log.Fatal(err)
			}
		}
	}
}
