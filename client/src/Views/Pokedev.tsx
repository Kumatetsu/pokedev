import React from 'react';
import { connect } from 'react-redux'
import { loadDevsAsync, deleteDevAsync } from '../store/dev/actions'
import TableDev from '../components/allDevsComponent'
import Button from '@material-ui/core/Button';
import {
    withRouter, RouteComponentProps
} from 'react-router-dom'
import { ThunkDispatch } from 'redux-thunk';
import { DevState, DevActionTypes } from '../store/dev/types';
import { IdentifiedType } from '../models/utils';
import { Dev } from '../models/devs';

interface props extends RouteComponentProps<any> {
    devs: IdentifiedType<Dev>[],
    load: () => Promise<void>,
    delete: (dev: IdentifiedType<Dev>) => Promise<void>
}

class Pokedev extends React.Component<props> {

    componentDidMount() {
        if (this.props.devs.length === 0) {
            this.props.load()
        }
    }

    render() {
        return <div>
            <h2>All devs</h2>
            <Button variant="contained" color="primary" style={{"display": "block"}} onClick={ () => {
                this.props.history.push('/create')
            }}>
                Create
            </Button>
            {
                0 < this.props.devs.length ?
                <TableDev devs={this.props.devs} deleteAction={this.props.delete}/> :
                <h2>No devs for the moment</h2>
            }
        </div>;
    }
}

const mapStateToProps = (state:{devReducer: DevState}) => {
    return {
        devs: state.devReducer.devs
    }
}

const mapDispatchToProps = (dispatch:ThunkDispatch<DevState, {}, DevActionTypes>) => ({
    load: () => dispatch(loadDevsAsync()),
    delete: (dev:IdentifiedType<Dev>) => dispatch(deleteDevAsync(dev))
})

export default withRouter<any, any>(connect(
    mapStateToProps,
    mapDispatchToProps
)(Pokedev))
