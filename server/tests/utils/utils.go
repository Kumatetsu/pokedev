package utils

import (
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
)

//CreateContext for tests with a recorder
func CreateContext(t *testing.T, method string, url string, body io.Reader) (*http.Request, *httptest.ResponseRecorder) {
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		t.Fatal(err)
	}
	recorder := httptest.NewRecorder()

	return req, recorder
}

//CompareStrings compare 2 string results
func CompareStrings(t *testing.T, body string, expected string) {
	if body != expected {
		t.Errorf("handler returned unexpected body: got %v want %v", body, expected)
	}
}
