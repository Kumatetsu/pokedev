## Stack:
    - Front: Reactjs/typescript
    - Back: Golang

## Commands:

    Launch dockers: docker-compose up -d --build


    go to:
        `localhost:3001` for web app
        `localhost:8001` for api

    while docker is running:
        - Launch unit tests : docker exec -it golang_app bash -c "go test -v ./tests/unit"
        - Launch functionnal tests:  docker exec -it golang_app bash -c "go test -v ./tests/functionnal"


## Projet:

    Dans ce projet j'ai voulu mettre en avant ce que je pouvais faire avec des langages que j'apprécie mais que je ne pratique pas au quotidien.
    L'idée projet de base est très simple: Un pokedex de développeurs.

    ### API:
        J'ai essayé de faire celle-ci en tdd (première fois que j'utilisais cette méthode). Tous les tests ont donc été fait en amont le plus possible et les fonctionnalités ajoutées après.

    ### CLIENT:
        Dans celui-ci j'ai utilisé redux pour la page de listing des devs (/pokedev)

        J'avais également envie de découvrir les hooks et plutôt que de faire de nombreux class Components avec redux j'ai préféré
        montrer une mise en place de hooks sur certaines views (/create, /elements)

    ### DOCKER:
        Pour faciliter le test de l'application j'ai souhaité dockeriser celle-ci (voir les commandes en haut du README)

    ### AMELIORATIONS:
        front:
        - tests front: n'en ayant jamais fait et en fonction du temps restant pour le projet j'ai préféré mettre en avant l'application client. J'aurais surement utilisé Jest pour effectuer ces tests.
        - gestion des erreurs front: celles ci sont soit set dans une variable grace aux hooks soit juste envoyées dans les logs console pour le moment.
        - meilleure utilisation du store et des hooks: ayant voulu tester les hooks, ceux-ci viennent court-circuiter des fois les possibilités du store (par exemple à la création d'un dev '/create')

        back:
        - gestion des données de tests: pour le moment les tests s'impactent les uns les autres. Pour pallier à ce problème j'aurais surement cherché à créer des transactions pour pouvoir rollback après chacun des tests.
        
## troubleshooting

    - Si aucun élément ne s'affiche sur la page /pokedev c'est surement que le docker de l'api n'a pas réussi à se connecter à la bdd. Il suffit de relancer le docker-compose. 
    Bien vérifier que les ports utilisés sont disponibles.
    - Pour voir les devs créés il faut refresh la page, car mon formulaire n'ajoute pas le nouveau dev au store (hooks/store)