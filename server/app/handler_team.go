package app

import (
	"encoding/json"
	"net/http"
	"pokedev/server/dao"
	"pokedev/server/models"
	"strconv"

	"github.com/gorilla/mux"
)

func (a *App) getAllTeams(w http.ResponseWriter, r *http.Request) {
	teamDao := dao.TeamDao{}
	teams, err := teamDao.All(a.Db)

	if nil != err {
		errorResponse(w, http.StatusBadRequest, err.Error())
		return
	}

	jsonResponse(w, http.StatusOK, teams)
}

func (a *App) getTeam(w http.ResponseWriter, r *http.Request) {
	teamDao := dao.TeamDao{}

	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, err.Error())
		return
	}

	team, err := teamDao.GetOneByID(a.Db, id)
	if nil != err {
		errorResponse(w, http.StatusBadRequest, err.Error())
		return
	}

	jsonResponse(w, http.StatusOK, team)
}

func (a *App) postTeam(w http.ResponseWriter, r *http.Request) {
	teamDao := dao.TeamDao{}
	decoder := json.NewDecoder(r.Body)

	var newTeam models.Team

	err := decoder.Decode(&newTeam)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, err.Error())
		return
	}

	newTeam, err = teamDao.Create(a.Db, newTeam)
	if nil != err {
		errorResponse(w, http.StatusBadRequest, err.Error())
		return
	}

	jsonResponse(w, http.StatusOK, newTeam)
}

func (a *App) putTeam(w http.ResponseWriter, r *http.Request) {
	teamDao := dao.TeamDao{}
	decoder := json.NewDecoder(r.Body)

	var updateTeam models.Team

	err := decoder.Decode(&updateTeam)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, err.Error())
		return
	}

	updateTeam, err = teamDao.Update(a.Db, updateTeam)
	if nil != err {
		errorResponse(w, http.StatusBadRequest, err.Error())
		return
	}

	jsonResponse(w, http.StatusOK, updateTeam)
}

func (a *App) deleteTeam(w http.ResponseWriter, r *http.Request) {
	teamDao := dao.TeamDao{}
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, err.Error())
		return
	}

	err = teamDao.Delete(a.Db, id)
	if nil != err {
		errorResponse(w, http.StatusBadRequest, err.Error())
		return
	}

	noContentResponse(w)
}
