package datas

var RequestNewDev = []byte(`{
	"firstname":"Petit",
	"lastname":"Nouveau",
	"elements":[{"id":1,"name":"developper"}],
	"skills":[{"id":5,"name":"php","power":8}]
}`)

var RequestUpdateNewDev = []byte(`{
	"id":4,
	"firstname":"Petit",
	"lastname":"Nouveau",
	"elements":[{"id":1,"name":"developper"}],
	"skills":[{"id":4,"name":"docker","power":10},{"id":5,"name":"php","power":8}]
}`)

var RequestNewElement = []byte(`{
	"name":"NeoDevelopper"
}`)

var RequestUpdateNewElement = []byte(`{
	"id": 5,
	"name":"TrinityDevelopper"
}`)

var RequestNewSkill = []byte(`{
	"name":"python",
	"power": 7
}`)

var RequestUpdateNewSkill = []byte(`{
	"id": 6,
	"name":"snake",
	"power": 7
}`)

var RequestNewTeam = []byte(`{
	"name": "caramel",
	"members": []
}`)

var RequestUpdateNewTeam = []byte(`{
	"id": 3,
	"name": "caramel",
	"members": [
		{
			"id":1,
			"firstname":"Jean",
			"lastname":"Billaud",
			"elements":[
				{
					"id":1,
					"name":"developper"
				},
				{
					"id":2,
					"name":"devops"
				}
			],
			"skills":[
				{
					"id":1,
					"name":"golang",
					"power":5
				},
				{
					"id":3,
					"name":"vuejs",
					"power":6
				}
			]
		}
	]
}`)
