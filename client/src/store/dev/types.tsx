import { IdentifiedType } from '../../models/utils';
import { Dev } from '../../models/devs';

export const ADD_DEV = 'ADD_DEV'
export const DELETE_DEV = 'DELETE_DEV'
export const SET_DEVS = 'SET_DEVS'

export interface DevState {
    devs: IdentifiedType<Dev>[]
}

interface AddDevAction {
    type: typeof ADD_DEV
    payload: Dev
}

interface SetDevsAction {
    type: typeof SET_DEVS
    payload: Dev[]
}

interface DeleteDevAction {
    type: typeof DELETE_DEV
    payload: IdentifiedType<Dev>
}

export type DevActionTypes = AddDevAction | DeleteDevAction | SetDevsAction

export type DevActionTypesOrVoid = DevActionTypes | void
