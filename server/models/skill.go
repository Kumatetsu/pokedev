package models

//Skill is the struc that contain the dev skills
type Skill struct {
	ID    int    `json:"id,omitempty"`
	Name  string `json:"name"`
	Power int    `json:"power"`
}
