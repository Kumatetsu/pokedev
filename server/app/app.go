package app

import (
	"fmt"

	//db driver library import
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
)

//App is container for our app
type App struct {
	Router *mux.Router
	Db     *gorm.DB
}

//Initialize db connection & router for the app
func (a *App) Initialize(user, password, dbname string) {
	connectionString := fmt.Sprintf("%s:%s@tcp(db:3306)/%s", user, password, dbname)

	var err error
	a.Db, err = gorm.Open("mysql", connectionString)
	if err != nil {
		panic("failed to connect database")
	}

	a.Router = a.initRouter()
}
