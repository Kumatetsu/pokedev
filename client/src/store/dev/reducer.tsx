import { DevActionTypes, DevState } from './types'
import { IdentifiedType } from '../../models/utils';
import { Dev } from '../../models/devs';

const initialState: DevState = {
  devs: []
}

const pokeReducer = (state = initialState, action: DevActionTypes) : DevState => {
    switch (action.type) {
        case 'ADD_DEV':
            return state
        case 'DELETE_DEV':
            const devs = state.devs.filter( (dev: IdentifiedType<Dev>) => {
                return dev.id !== action.payload.id
            });
            let update = {...state}
            update.devs = devs
            return update
        case 'SET_DEVS':
            return Object.assign({}, state,  {devs: action.payload})
        default:
            return state
    }
}


export default pokeReducer
